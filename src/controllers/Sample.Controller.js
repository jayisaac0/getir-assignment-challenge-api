const express = require('express');

class SampleController {
    static async fetch(req, res) {
        /** code */
        res.send('Hello World!')
    }
}

module.exports = SampleController;