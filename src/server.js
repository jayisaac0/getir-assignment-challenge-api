const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
require("dotenv").config();
require('./config/connection')
const routing = require('./routes')

const app = express()
app.use(cors());


routing(app)

app.listen(process.env.PORT, () => {
    console.log(`api listening on port ${process.env.PORT}!`)
})
