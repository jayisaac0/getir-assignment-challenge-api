const express = require('express');
const router = express.Router();
const SampleController = require('../../controllers/Sample.Controller');

module.exports = (app) => {

    router.post('/fetch', SampleController.fetch)

    app.use('/api/v1/sample', router);
}