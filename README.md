# getir-assignment-challenge-api

Getir assignment challenge creating a Restful API with a single endpoint that fetches the data in provided MongoDB collection and return the results in the requested format.

View complete documentation by visiting the link [Api documentation](https://gitlab.com/jayisaac0/getir-assignment-challenge-api/-/wikis/home)